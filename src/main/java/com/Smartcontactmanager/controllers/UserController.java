package com.Smartcontactmanager.controllers;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.Principal;
import java.time.LocalDateTime;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.Smartcontactmanager.Helper.Message;
import com.Smartcontactmanager.entities.Contact;
import com.Smartcontactmanager.entities.User;
import com.Smartcontactmanager.service.UserService;


@Controller
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	UserService userService;
	
	User currentLogInUserDetails = null;
	
	@ModelAttribute
	public void commonUserData(Model model, Principal principal) {
	
		String userName = principal.getName();
		System.out.println("Logged In Username :"+userName);
		User currentLogInUserDetails = userService.findUserByEmail(userName);
		System.out.println(currentLogInUserDetails);
		model.addAttribute("user", currentLogInUserDetails);
		this.currentLogInUserDetails = currentLogInUserDetails;
	}

	/* User home handler*/
	@RequestMapping("/index")
	public String dashboard(Model model, Principal principal ) {
		
		model.addAttribute("title", "User Dashboard");
		return "user/user_dashboard";
	
	}
	
	@GetMapping("/add-contact-form")
	public String addUserContact(Model model) {
			
		model.addAttribute("title", "Add contact : Smart contact Manager");
		model.addAttribute("contact",new Contact());	
		return "user/add_contact_form";
	
	}
	

	@PostMapping("/process-contact")
	public String processAddContact(@Valid @ModelAttribute Contact contact, BindingResult result ,@RequestParam("profileImage") MultipartFile mpFile,Principal principal, Model model, HttpSession session) {

		Path destPath = null;
		String originalFilename = null;
		
		String currDateTime= (LocalDateTime.now()+"").replace(":", "-");
		
		try {

			
			if(mpFile.isEmpty()) {
				System.out.println("file is empty");
				originalFilename = "contact_profile.png";
			}else {
				originalFilename = currDateTime+"@"+mpFile.getOriginalFilename();
			}	
				 File savedFile = new ClassPathResource("/static/image").getFile();
			 
				 destPath = Paths.get(savedFile.getAbsolutePath()+File.separator+originalFilename);
				 System.out.println("Image path :"+destPath);
				 
				contact.setImage(originalFilename);
			
			contact.setUser(currentLogInUserDetails);
			
			currentLogInUserDetails.getContacts().add(contact);	
			
		User addedContactResult = userService.addContactInUser(currentLogInUserDetails);
		
		if(addedContactResult !=null) {
			
			Files.copy(mpFile.getInputStream(), destPath, StandardCopyOption.REPLACE_EXISTING);
			System.out.println("After successful contact added : "+addedContactResult);
		}
		
		session.setAttribute("message", new Message("Contact saved successfully.....!!", "success"));
		model.addAttribute("contact", new Contact());
		return "user/add_contact_form";
		
		}catch(Exception e) {
			
			System.out.println("Error : "+e);
			e.printStackTrace();
			model.addAttribute("contact", contact);

			session.setAttribute("message", new Message("Something goes wrong, please try again.....!!", "danger"));
			return "user/add_contact_form";
		}

	}
	
	@GetMapping("/show-contacts/{page}")
	public String showContacts(@PathVariable("page") Integer page,  Model model, Principal principal) {
		
		String currentUser = principal.getName();
		User currentUserDetails = this.userService.findUserByEmail(currentUser);
		
		Pageable pageable = PageRequest.of(page, 5);	
		
		Page<Contact> contacts = this.userService.getContactsList(currentUserDetails.getId(), pageable);	
		System.out.println(contacts.getTotalPages());
		
		model.addAttribute("titile", "Show contacts - Smart contact Manager");
		model.addAttribute("contacts", contacts);
		model.addAttribute("currentPage", page);
		model.addAttribute("totalPages", contacts.getTotalPages());
		
		
		
		return "user/show_contacts";
		
	}
	
	@SuppressWarnings("unlikely-arg-type")
	@GetMapping("/{cId}/contact")
	public String showContact(@PathVariable("cId") int cId, Principal principal, Model model ) {
		System.out.println("CID : "+cId);
		
		String currentUser = principal.getName();
		model.addAttribute("title", "Contact details : Smart contact Manager");
		
		Contact contactDetail = this.userService.getContactDetail(cId);
		
		if(! currentUser.equals(contactDetail.getUser().getEmail()))
			model.addAttribute("message", new Message("You are not an authorized user for this contact", "denger"));
		else
			model.addAttribute("contact", contactDetail);
		
		return "user/show_user_contact_details";
	}
	
	@GetMapping("/delete-contact/{cId}")
	public String deleteContact(@PathVariable("cId") Integer cId, Principal principal, Model model ) {
		
		String name = principal.getName();
		User currentUser = this.userService.findUserByEmail(name);
		
		Contact resultContact = this.userService.getContactById(cId);
		
		
		if(currentUser.getId() == resultContact.getUser().getId()) {
			
			this.userService.deleteContact(currentUser, resultContact);
			
			// CODE HERE
			
		}else {
			model.addAttribute("message", new Message("You are not an authorized user for this contact", "denger"));
		}
		return "redirect:/user/show-contacts/0";
	}
	
	@GetMapping("/update-contact/{cId}")
	public String updateContact(@PathVariable("cId") Integer cId, Model model ) {
		Contact contact = this.userService.getContactById(cId);
		
		model.addAttribute("title", "Update contact - Smart Contact Manager");
		model.addAttribute("subTitle", "Update your Contact");
		model.addAttribute("contact", contact);
		
		return "user/update_contact";
	}

	
	@PostMapping("/process-update-contact")
	public String processUpdateContact(@ModelAttribute Contact contact, @RequestParam("profileImage") MultipartFile file, Model model, Principal principal, HttpSession session) {
		
		 Contact oldContact = this.userService.getContactById(contact.getCId());
		
		try {
		
		 User currentUser = this.userService.findUserByEmail(principal.getName());
				 
		 contact.setUser(currentUser);
		
		 File saveFile = new ClassPathResource("/static/image").getFile();
		 
			String uniqueImageName = (LocalDateTime.now()+"").replace(":", "-")+"@"+file.getOriginalFilename();
		 
		if(!file.isEmpty()) {
			

			// first delete previous photo from DB and from saved folder
			if(oldContact.getImage() !=null) {
				File deleteFile = new File(saveFile, oldContact.getImage());
				deleteFile.delete();
			}
			
			// Update new image into our folder location
			Path path = Paths.get(saveFile+File.separator+uniqueImageName);
			Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
			contact.setImage(uniqueImageName);
			
		}else {
			
			
			if(oldContact.getImage() !=null)
				contact.setImage(oldContact.getImage());
			else
				contact.setImage("contact_profile.png");
			
		}
			
		
			Contact updatedContact = this.userService.updateContactInUser(contact);
			session.setAttribute("message", new Message("Contact successfully updated", "success"));
		} catch (Exception e) {
			session.setAttribute("message", new Message("Contact updation failed ", "danger"));
			e.printStackTrace();
			model.addAttribute("contact", oldContact);
			return "redirect:/user/update-contact/"+contact.getCId();
		}
		
		
		System.out.println("contact name : "+contact.getName());
		System.out.println("contact ID : "+contact.getCId());
		return "redirect:/user/"+contact.getCId()+"/contact";
	}
	
	@GetMapping("/profile")
	public String showUserProfile() {
		
		return "user/profile";
	}
	
	
	
	
	
	
}