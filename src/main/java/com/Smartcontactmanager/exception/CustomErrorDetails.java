package com.Smartcontactmanager.exception;

import java.sql.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomErrorDetails {
    private String message;
	private Date date;
	private String details;
    public CustomErrorDetails(String message, Date date, String details) {
		super();
		this.message = message;
		this.date = date;
		this.details = details;
	}
}
