package com.Smartcontactmanager.entities;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name="contacts")
public class Contact {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int cId;
	
	@NotBlank(message = "Name is mandatory")
	private String name;
	
	@NotBlank(message="Nickname not be blank")
	private String secondName;
	
	@NotBlank(message="Work field not be blank")
	private String work;
	
	@NotBlank(message="Email is mandatory ")
	private String email;
	
	private String image;
	
	@NotBlank(message="Phone number is mandatory")
	private String phone;
	
	@NotBlank(message = "Description must not be blank")
	@Column(length = 5000)
	private String description;
	
	
	@ManyToOne
	private User user;
	
	public Contact() {
		
	}

	/*
	@Override
	public String toString() {
		return "Contact [cId=" + cId + ", name=" + name + ", secondName=" + secondName + ", work=" + work + ", email="
				+ email + ", image=" + image + ", description=" + description + ", phone=" + phone + ", user=" + user
				+ "]";
	}	
*/
	
	@Override
	public boolean equals(Object obj) {
		return this.cId == ((Contact)obj).getCId();
	}
	
}